package OSI.OPE.MSI.OEI.SOI.SMQ.save;

import java.io.File;
import java.io.FileWriter;

import OSI.OPE.OVU.MVU.OVU.PQE.nodeEdit.LinkNode;

public class SaveAnd_U_File{
	public static void update(String fileCurrentpath, LinkNode first) {
		//delete file 
		File file= new File(fileCurrentpath);
		if(file.exists()&& file.isFile()) {
			file.delete();
		}
		//save
		String fileSavepath= fileCurrentpath;
		System.out.println(fileSavepath);
		//create file and save
		try {
			FileWriter fileWriter= new FileWriter(fileSavepath);
			LinkNode node= first;
			while(null!= node) {
				//挨个取。没难度。逐个把信息写入文件。
				//节点坐标，节点名， 节点关联，
				String NodeCoordinationX= StableCommon.STRING_EMPTY node.x;
				String NodeCoordinationY= StableCommon.STRING_EMPTY node.y;
				String NodeName= StableCommon.STRING_EMPTY node.name;
				String NodeID=""+ node.ID;
				String flash=""+ node.flash;
				String beconnect= StableCommon.STRING_EMPTY node.beconnect;
				String leftChoose= StableCommon.STRING_EMPTY node.leftChoose;
				String rightChoose= StableCommon.STRING_EMPTY node.rightChoose;
				String tBeconnect= StableCommon.STRING_EMPTY node.tBeconnect;
				String tBeconnectX= StableCommon.STRING_EMPTY node.tBeconnectX;
				String tBeconnectY= StableCommon.STRING_EMPTY node.tBeconnectY;
				String tBeconnectName= StableCommon.STRING_EMPTY node.tBeconnectName;
				String tBeconnectID= StableCommon.STRING_EMPTY node.tBeconnectID;
				String tBeconnectPrimaryKey= StableCommon.STRING_EMPTY node.dBeconnectPrimaryKey;
				String mBeconnect= StableCommon.STRING_EMPTY node.mBeconnect;
				String mBeconnectX= StableCommon.STRING_EMPTY node.mBeconnectX;
				String mBeconnectY= StableCommon.STRING_EMPTY node.mBeconnectY;
				String mBeconnectName= StableCommon.STRING_EMPTY node.mBeconnectName;
				String mBeconnectID= StableCommon.STRING_EMPTY node.mBeconnectID;
				String mBeconnectPrimaryKey= StableCommon.STRING_EMPTY node.mBeconnectPrimaryKey;
				String dBeconnect= StableCommon.STRING_EMPTY node.dBeconnect;
				String dBeconnectX= StableCommon.STRING_EMPTY node.dBeconnectX;
				String dBeconnectY= StableCommon.STRING_EMPTY node.dBeconnectY;
				String dBeconnectName= StableCommon.STRING_EMPTY node.dBeconnectName;
				String dBeconnectID= StableCommon.STRING_EMPTY node.dBeconnectID;
				String dBeconnectPrimaryKey= StableCommon.STRING_EMPTY node.dBeconnectPrimaryKey;
				String primaryKey= StableCommon.STRING_EMPTY node.primaryKey;
				String nodeConfiguration= StableCommon.STRING_EMPTY node.thisFace.nodeConfiguration;
				String isConfiged= StableCommon.STRING_EMPTY node.thisFace.isConfiged;
				String isExecuted= StableCommon.STRING_EMPTY node.thisFace.isExecuted;
				//配置
				fileWriter.write("\r\n");
				fileWriter.write("NodeCoordinationX:"+ NodeCoordinationX);
				fileWriter.write("\r\n");
				fileWriter.write("NodeName:"+ NodeName);
				fileWriter.write("\r\n");
				fileWriter.write("NodeCoordinationY:"+ NodeCoordinationY);
				fileWriter.write("\r\n");
				fileWriter.write("NodeID:"+ NodeID);
				fileWriter.write("\r\n");
				fileWriter.write("flash:"+ flash);
				fileWriter.write("\r\n");
				fileWriter.write("beconnect:"+ beconnect);
				fileWriter.write("\r\n");
				fileWriter.write("leftChoose:"+ leftChoose);
				fileWriter.write("\r\n");
				fileWriter.write("rightChoose:"+ rightChoose);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnect:"+ tBeconnect);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnectX:"+ tBeconnectX);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnectY:"+ tBeconnectY);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnectName:"+ tBeconnectName);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnectID:"+ tBeconnectID);
				fileWriter.write("\r\n");
				fileWriter.write("tBeconnectPrimaryKey:"+ tBeconnectPrimaryKey);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnect:"+ mBeconnect);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnectX:"+ mBeconnectX);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnectY:"+ mBeconnectY);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnectName:"+ mBeconnectName);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnectID:"+ mBeconnectID);
				fileWriter.write("\r\n");
				fileWriter.write("mBeconnectPrimaryKey:"+ mBeconnectPrimaryKey);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnect:"+ dBeconnect);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnectX:"+ dBeconnectX);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnectY:"+ dBeconnectY);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnectName:"+ dBeconnectName);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnectID:"+ dBeconnectID);
				fileWriter.write("\r\n");
				fileWriter.write("dBeconnectPrimaryKey:"+ dBeconnectPrimaryKey);
				fileWriter.write("\r\n");
				fileWriter.write("primaryKey:"+ primaryKey);
				fileWriter.write("\r\n");
				fileWriter.write("nodeConfiguration:"+ nodeConfiguration);
				fileWriter.write("\r\n");
				fileWriter.write("isConfiged:"+ isConfiged);
				fileWriter.write("\r\n");
				fileWriter.write("isExecuted:"+ isExecuted);
				fileWriter.write("\r\n");
				//分割
				String split="##############################";
				fileWriter.write("\r\n");
				fileWriter.write(split);
				fileWriter.flush();
				if(null== node.next) {
					break;
				}
				node= node.next;
			}	
			fileWriter.close();
		}catch(Exception saveFile) {

		}
	}
}