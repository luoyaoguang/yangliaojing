package OSI.OPE.SI.SD.SU.SQ.ASU.OSU.PSU.MSU.AVQ.ASQ.life;

import java.util.concurrent.CopyOnWriteArrayList;

import OSI.OPE.SI.SD.SU.SQ.ASU.OSU.PSU.MSU.AVQ.ASQ.PP.LoveProcessIssues;
import OSI.OPE.SI.SD.SU.SQ.ASU.OSU.PSU.MSU.AVQ.ASQ.analysis.LoveAnalysisIssues;
import OSI.OPE.SI.SD.SU.SQ.ASU.OSU.PSU.MSU.AVQ.ASQ.management.LoveManagementIssues;
import OSI.OPE.SI.SD.SU.SQ.ASU.OSU.PSU.MSU.AVQ.ASQ.operation.Love_O_Issues;

public class LoveIssues{
	public void loveDefinition() {

	}
	public void loveImplementation() {

	}
	public void loveCombination() {

	}
	public void loveExtension() {

	}
	public void loveAckquisition() {

	}
	public void philosothon(CopyOnWriteArrayList<String> read) {
		//a
		new LoveAnalysisIssues().vpcsInitons();
		//o
		new Love_O_Issues().vpcsInitons();
		//p
		new LoveProcessIssues().vpcsInitons();
		//m
		new LoveManagementIssues().vpcsInitons();
	}
}