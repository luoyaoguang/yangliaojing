package ME.SM.OP.SM.AOP.MEC.SIQ.E;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import SVQ.stable.StableShellETL;
@SuppressWarnings({"unused","unchecked"})
public class P_RelationPLSQL {
	public static void P_AndMap(String[] sets, List<Map<String, Object>> obj
			, List<Map<String, Object>> joinObj
			, Map<String, Object> object, List<Map<String, Object>> newObj) {
		List<Map<String, Object>> newObjTemp = new ArrayList<>();
		Iterator<Map<String, Object>> iterator = newObj.iterator(); 
		int count = 0;
		while(iterator.hasNext()) {
			int objRowId = count++;
			Map<String, Object> objRow = iterator.next();
			if(objRow.containsKey(sets[0])&&objRow.containsKey(sets[2])) {
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_DOUBLE_EQUALS) || sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_TRIPAL_EQUALS)) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							== new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_NOT_EUQAL_TO) || sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_NOT_EUQAL_TO_R) 
						|| sets[1].equalsIgnoreCase("<>") || sets[1].equalsIgnoreCase("><")) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							!= new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_GREATER_AND_EQUAL_TO) || sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_GREATER_AND_EQUAL_TO_R)) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							>= new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_GREATER)) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							> new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_SMALL)) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							< new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_LESS_AND_EQUAL_TO) || sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_LESS_AND_EQUAL_TO)) {
					if(new BigDecimal(objRow.get(sets[0]).toString()).doubleValue() 
							<= new BigDecimal(objRow.get(sets[2]).toString()).doubleValue()) {
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_EQUAL)) {
					if(objRow.get(sets[0]).toString().equals(objRow.get(sets[2]).toString())){
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_DOES_NOT_EQUALS) || sets[1].equalsIgnoreCase("equal!")) {
					if(!objRow.get(sets[0]).toString().equals(objRow.get(sets[2]).toString())){
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase((StableShellETL.SHELL_ETL_IN))) {
					String set = "," + objRow.get(sets[2]).toString() + ",";
					if(set.contains(objRow.get(sets[0]).toString())){
						newObjTemp.add(objRow);
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_NOT_IN)) {
					String set = "," + objRow.get(sets[2]).toString() + ",";
					if(!set.contains(objRow.get(sets[0]).toString())){
						newObjTemp.add(objRow);
					}
				}
			}
		}
	}

	public static void P_OrMap(String[] sets, List<Map<String, Object>> obj
			, List<Map<String, Object>> joinObj
			, Map<String, Object> object, List<Map<String, Object>> newObj
			, Map<String, Boolean> findinNewObj) {
		Iterator<Map<String, Object>> iterator = obj.iterator(); 
		int count = 0;
		while(iterator.hasNext()) {
			int objRowId = count++;
			Map<String, Object> objRow = iterator.next();
			Map<String, Object> row = (Map<String, Object>) objRow.get(StableShellETL.SHELL_ETL_ROWVALUE);
			Iterator<Map<String, Object>> iteratorJoin = joinObj.iterator(); 
			int countJoin = 0;
			while(iteratorJoin.hasNext()) {
				int objJoinRowId = countJoin++;
				Map<String, Object> objJoinRow = iteratorJoin.next();
				Map<String, Object> joinRow = (Map<String, Object>) objJoinRow.get(StableShellETL.SHELL_ETL_ROWVALUE);
				Map<String, Object> cell = (Map<String, Object>) row.get(sets[0]);
				Map<String, Object> cellJoin = (Map<String, Object>) joinRow.get(sets[2]);
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_DOUBLE_EQUALS) || sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_TRIPAL_EQUALS)) {
					if(new BigDecimal(cell.get("culumnValue").toString()).doubleValue() 
							== new BigDecimal(cellJoin.get("culumnValue").toString()).doubleValue()) {
						if(!findinNewObj.containsKey(objRowId + ":" + objJoinRowId)) {
							Map<String, Object> newObjRow = new HashMap<>();
							Map<String, Object> newRow = new HashMap<>();
							newRow.putAll((Map<? extends String, ? extends Object>) objJoinRow.get(StableShellETL.SHELL_ETL_ROWVALUE));
							newRow.putAll((Map<? extends String, ? extends Object>) objRow.get(StableShellETL.SHELL_ETL_ROWVALUE));	 
							newObjRow.put(StableShellETL.SHELL_ETL_ROWVALUE, newRow);
							newObj.add(newObjRow);
							findinNewObj.put(objRowId + ":" + objJoinRowId, true);
						} 
					}
				}
				if(sets[1].equalsIgnoreCase(StableShellETL.SHELL_ETL_EQUAL)) {
					if(cell.get("culumnValue").toString().equals(cellJoin.get("culumnValue").toString())) {
						if(!findinNewObj.containsKey(objRowId + ":" + objJoinRowId)) {
							Map<String, Object> newObjRow = new HashMap<>();
							Map<String, Object> newRow = new HashMap<>();
							newRow.putAll((Map<? extends String, ? extends Object>) objJoinRow.get(StableShellETL.SHELL_ETL_ROWVALUE));
							newRow.putAll((Map<? extends String, ? extends Object>) objRow.get(StableShellETL.SHELL_ETL_ROWVALUE));	 
							newObjRow.put(StableShellETL.SHELL_ETL_ROWVALUE, newRow);
							newObj.add(newObjRow);
							findinNewObj.put(objRowId + ":" + objJoinRowId, true);
						} 
					}
				}
			}
		}
	}
}